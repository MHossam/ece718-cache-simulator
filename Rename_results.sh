#!/bin/bash

mv BMs/eembc-traces/a2time01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/a2time01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/a2time01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/a2time01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/a2time01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/a2time01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/a2time01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/a2time01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/aifirf01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/aifirf01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/aifirf01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/aifirf01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/aifirf01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/aifirf01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/aifirf01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/aifirf01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/basefp01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/basefp01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/basefp01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/basefp01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/basefp01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/basefp01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/basefp01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/basefp01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/cacheb01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/cacheb01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/cacheb01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/cacheb01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/cacheb01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/cacheb01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/cacheb01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/cacheb01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/empty-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/empty-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/empty-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/empty-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/empty-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/empty-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/empty-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/empty-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/iirflt01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/iirflt01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/iirflt01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/iirflt01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/iirflt01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/iirflt01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/iirflt01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/iirflt01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/pntrch01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/pntrch01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/pntrch01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/pntrch01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/pntrch01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/pntrch01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/pntrch01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/pntrch01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/rspeed01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/rspeed01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/rspeed01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/rspeed01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/rspeed01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/rspeed01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/rspeed01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/rspeed01-trace/newLogger/MSI_inOrder_C3.csv

mv BMs/eembc-traces/ttsprk01-trace/newLogger/LatencyReport_C0.csv BMs/eembc-traces/ttsprk01-trace/newLogger/MSI_inOrder_C0.csv
mv BMs/eembc-traces/ttsprk01-trace/newLogger/LatencyReport_C1.csv BMs/eembc-traces/ttsprk01-trace/newLogger/MSI_inOrder_C1.csv
mv BMs/eembc-traces/ttsprk01-trace/newLogger/LatencyReport_C2.csv BMs/eembc-traces/ttsprk01-trace/newLogger/MSI_inOrder_C2.csv
mv BMs/eembc-traces/ttsprk01-trace/newLogger/LatencyReport_C3.csv BMs/eembc-traces/ttsprk01-trace/newLogger/MSI_inOrder_C3.csv