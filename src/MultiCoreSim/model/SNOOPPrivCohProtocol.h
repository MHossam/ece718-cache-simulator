/*
 * File  :      SNOOPPrivCohProtocol.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On May 29, 2020
 */

#ifndef _SNOOPPrivCohProtocol_H
#define _SNOOPPrivCohProtocol_H

#include "ns3/ptr.h"
#include "ns3/object.h"
#include "ns3/core-module.h"

#include "CohProtocolCommon.h"

namespace ns3
{

  class SNOOPPrivCohProtocol : public ns3::Object
  {
  private:
    
    int m_coreId;
    int m_sharedMemId;
    bool m_cache2Cache;

    int m_currEventCurrState;
    int m_currEventNextState;

    int m_prllActionCnt;
    int m_reqWbRatio;

    bool m_logFileGenEnable;

    CohProtType m_pType;
    IFCohProtocol *m_fsm_ptr;

    Ptr<GenericCache> m_privCache;
    CpuFIFO* m_cpuFIFO;
    BusIfFIFO* m_busIfFIFO;

    SNOOPPrivEventList m_eventList;
    SNOOPPrivMsgList m_msgList;
    SNOOPPrivEventsCacheInfo m_eventCacheInfoList;
    
    SNOOPPrivCohTrans m_currEventTrans2Issue;
    SNOOPPrivEventType m_processEvent;
    SNOOPPrivCtrlAction m_ctrlAction;

    ReplcPolicy m_replcPolicy;
    Ptr<UniformRandomVariable> uRnd1;


    // EventID getEvent(CpuFIFO::ReqMsg *cpu_msg);
    // EventID getEvent(BusIfFIFO::BusReqMsg *bus_req_msg);
    // EventID getEvent(BusIfFIFO::BusRespMsg *bus_resp_msg);
  public:

    SNOOPPrivCohProtocol();
    SNOOPPrivCohProtocol(CohProtType ptype,
                          bool logFileGenEnable, int coreId, int sharedMemId, bool cache2Cache, int reqWbRatio,
                          Ptr<GenericCache> privCache, CpuFIFO* cpuFIFO, BusIfFIFO* busFIFO);
    ~SNOOPPrivCohProtocol();

    static TypeId GetTypeId(void);
    CohProtType GetProtocolType();
    SNOOPPrivEventType GetCurrProcEvent();
    CpuFIFO::ReqMsg GetCpuReqMsg();
    BusIfFIFO::BusReqMsg GetBusReqMsg();
    BusIfFIFO::BusRespMsg GetBusRespMsg();
    SNOOPPrivCoreEvent GetCpuReqEvent();
    SNOOPPrivReqBusEvent GetBusReqEvent();
    SNOOPPrivRespBusEvent GetBusRespEvent();
    SNOOPPrivCtrlAction GetCurrEventCtrlAction();
    GenericCache::CacheLineInfo GetCurrEventCacheLineInfo();
    int GetCurrEventCacheCurrState();
    int GetCurrEventCacheNextState();
    SNOOPPrivCohTrans GetCurrEventCohrTrans();
    SNOOPPrivCohTrans GetCohTrans();

    void SetCpuReqMsg(CpuFIFO::ReqMsg cpuReqMsg);
    void SetCpuReqEvent(SNOOPPrivCoreEvent cpuReqEvent);
    void SetCurrEventCtrlAction(SNOOPPrivCtrlAction currEvent);

    void UpdateCacheLine(CacheField field,
                         GenericCacheFrmt cacheLineInfo,
                         uint32_t set_idx,
                         uint32_t way_idx);

    std::string PrintPrivEventType(SNOOPPrivEventType event);
    std::string PrintPrivActionName(SNOOPPrivCtrlAction action);
    std::string PrintTransName(SNOOPPrivCohTrans trans);
    std::string PrivReqBusEventName(SNOOPPrivReqBusEvent event);
    std::string PrivRespBusEventName(SNOOPPrivRespBusEvent event);

    int InvalidCacheState();
    void InitializeCacheStates();
    void ChkCohEvents();
    void GetEventsCacheInfo();
    void CohEventsSerialize();
    void ProcessCoreEvent();
    void ProcessBusEvents();
    void CohProtocolFSMProcessing();
    bool IsCacheBlkValid(int s);
    SNOOPPrivEventPriority PrivEventPriorityBinding(IFCohProtocol *obj);
    void CohProtocolFSMBinding(IFCohProtocol *obj);
    std::string PrintPrivStateName(int state);
    void PrintEventInfo();

    SNOOPPrivCtrlAction processRequest(CpuFIFO::ReqMsg *cpu_msg, 
                                        BusIfFIFO::BusReqMsg *bus_req_msg, 
                                        BusIfFIFO::BusRespMsg *bus_resp_msg);
  }; // class CohProtocol
}

#endif /* _SNOOPPrivCohProtocol_H */
