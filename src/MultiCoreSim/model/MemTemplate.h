/*
 * File  :      MemTemplate.h
 * Author:      Salah Hessien
 * Email :      salahga@mcmaster.ca
 *
 * Created On February 16, 2020
 */

#ifndef _MemTemplate_H
#define _MemTemplate_H

#include "CommunicationInterface.h"

#include "ns3/object.h"
#include <queue>
#include <string>

namespace ns3
{
  // Generic FIFO Interface
  template <typename T>
  class GenericFIFO : public ns3::Object
  {
  private:
    std::queue<T> m_FIFO;
    uint16_t m_fifoDepth;

  public:
    void SetFifoDepth(int fifoDepth)
    {
      m_fifoDepth = fifoDepth;
    }

    int GetFifoDepth()
    {
      return m_fifoDepth;
    }

    void InsertElement(T msg)
    {
      m_FIFO.push(msg);
    }

    void PopElement()
    {
      m_FIFO.pop();
    }

    T GetFrontElement()
    {
      return m_FIFO.front();
    }

    void UpdateFrontElement(T msg)
    {
      m_FIFO.front() = msg;
    }

    int GetQueueSize()
    {
      return m_FIFO.size();
    }

    bool IsEmpty()
    {
      return m_FIFO.empty();
    }

    bool IsFull()
    {
      return (m_FIFO.size() == m_fifoDepth) ? true : false;
    }
  };

  // CPU FIFO Interface
  class CpuFIFO : public CommunicationInterface
  {
  public:
    enum REQTYPE
    {
      READ = 0,
      WRITE = 1,
      REPLACE = 2
    };
    // A request  contains information on
    // its own memory request, type, and cycle.
    struct ReqMsg
    {
      uint64_t msgId;
      uint16_t reqCoreId;
      uint64_t addr;
      uint64_t cycle;
      uint64_t fifoInserionCycle;
      REQTYPE type;
      uint8_t data[8];
    };

    struct RespMsg
    {
      uint64_t msgId;
      uint64_t addr;
      uint64_t reqcycle;
      uint64_t cycle;
    };

  public:
    GenericFIFO<ReqMsg> m_txFIFO;
    GenericFIFO<RespMsg> m_rxFIFO;

    CpuFIFO(int id, int FIFOs_depth) : CommunicationInterface(id)
    {
      m_txFIFO.SetFifoDepth(FIFOs_depth);
    }

    virtual bool peekMessage(Message *out_msg)
    {
      if (!m_txFIFO.IsEmpty())
      {
        *out_msg = {.msg_id = m_txFIFO.GetFrontElement().msgId,
                    .requestor_id = m_txFIFO.GetFrontElement().reqCoreId,
                    .addr = m_txFIFO.GetFrontElement().addr,
                    .cycle = m_txFIFO.GetFrontElement().cycle,
                    .complementary_value = (uint64_t)m_txFIFO.GetFrontElement().type,
                    .data = NULL};
        return true;
      }
      else
        return false;
    }

    virtual void popFrontMessage()
    {
      m_txFIFO.PopElement();
    }

    virtual bool pushMessage(Message &msg, uint64_t cycle, double cycle_width)
    {
      RespMsg resp_msg = {.msgId = msg.msg_id,
                          .reqcycle = msg.cycle,
                          .addr = msg.addr,
                          .cycle = cycle};
      m_rxFIFO.InsertElement(resp_msg);

      return true;
    }
  };

  // Bus interface FIFO
  class BusIfFIFO : public CommunicationInterface
  {
  public:
    // A request  contains information on
    // its own memory request, type, and cycle.
    struct BusReqMsg
    {
      uint64_t addr;
      uint64_t msgId;
      uint16_t reqCoreId;
      uint16_t wbCoreId;
      uint16_t cohrMsgId;
      double timestamp;
      uint64_t cycle;
      bool NoGetMResp;
    };

    struct BusRespMsg
    {
      uint64_t addr;
      uint64_t msgId;
      bool dualTrans;
      bool execlusive_data;
      uint16_t reqCoreId;
      uint16_t respCoreId;
      double timestamp;
      uint64_t cycle;
      uint8_t data[8];
    };

  private:
    int m_fifo_selector;

  public:
    GenericFIFO<BusReqMsg> m_txMsgFIFO;
    GenericFIFO<BusRespMsg> m_txRespFIFO;
    GenericFIFO<BusReqMsg> m_rxMsgFIFO;
    GenericFIFO<BusRespMsg> m_rxRespFIFO;

    BusIfFIFO(int id, int FIFOs_depth) : CommunicationInterface(id)
    {
      m_fifo_selector = -1;
      m_txMsgFIFO.SetFifoDepth(FIFOs_depth);
      m_txRespFIFO.SetFifoDepth(FIFOs_depth);
      m_rxMsgFIFO.SetFifoDepth(FIFOs_depth);
      m_rxRespFIFO.SetFifoDepth(FIFOs_depth);
    }

    virtual bool peekMessage(Message *out_msg)
    {
      if (!m_rxMsgFIFO.IsEmpty() && m_fifo_selector != 0)
      {
        BusReqMsg rx_msg = m_rxMsgFIFO.GetFrontElement();
        *out_msg = {.msg_id = rx_msg.msgId,
                    .addr = rx_msg.addr,
                    .requestor_id = rx_msg.reqCoreId,
                    .responder_id = rx_msg.wbCoreId,
                    .complementary_value = rx_msg.cohrMsgId,
                    .cycle = rx_msg.cycle,
                    .data = NULL};
        m_fifo_selector = 0;
        return true;
      }

      if (!m_rxRespFIFO.IsEmpty() && m_fifo_selector != 1)
      {
        BusRespMsg rx_resp = m_rxRespFIFO.GetFrontElement();
        *out_msg = {.msg_id = rx_resp.msgId,
                    .addr = rx_resp.addr,
                    .requestor_id = rx_resp.reqCoreId,
                    .responder_id = rx_resp.respCoreId,
                    .cycle = rx_resp.cycle,
                    .complementary_value = (uint64_t)((rx_resp.execlusive_data) ? 2 : 0)};

        out_msg->data = new uint8_t[sizeof(rx_resp.data)];
        memcpy(out_msg->data, rx_resp.data, sizeof(rx_resp.data));
        m_fifo_selector = 1;
        return true;
      }

      m_fifo_selector = -1;
      return false;
    }

    virtual void popFrontMessage()
    {
      if (m_fifo_selector == 0)
        m_rxMsgFIFO.PopElement();
      else if (m_fifo_selector == 1)
        m_rxRespFIFO.PopElement();
    }

    virtual bool pushMessage(Message &msg, uint64_t cycle, double cycle_width)
    {
      if (msg.data != NULL && !m_txRespFIFO.IsFull())
      {
        BusRespMsg resp_msg = {.msgId = msg.msg_id,
                               .addr = msg.addr,
                               .reqCoreId = msg.requestor_id,
                               .respCoreId = msg.responder_id,
                               .timestamp = cycle * cycle_width,
                               .cycle = cycle,
                               .dualTrans = (msg.complementary_value == 1),
                               .execlusive_data = (msg.complementary_value == 2)};
        memcpy(resp_msg.data, msg.data, sizeof(resp_msg.data));

        m_txRespFIFO.InsertElement(resp_msg);
        return true;
      }
      else if (msg.data == NULL && !m_txMsgFIFO.IsFull())
      {
        BusReqMsg request_msg = {.msgId = msg.msg_id,
                                 .reqCoreId = msg.requestor_id,
                                 .wbCoreId = msg.responder_id,
                                 .cohrMsgId = (uint16_t)(msg.complementary_value - 2), //ToDo: subtraction should be removed in the future. It is here only to convert MSIProtocol::ActionId to SNOOPPrivCohTrans
                                 .addr = msg.addr,
                                 .timestamp = cycle * cycle_width,
                                 .cycle = cycle,
                                 .NoGetMResp = false};

        m_txMsgFIFO.InsertElement(request_msg);
        return true;
      }
      return false;
    }
  };

  class DRAMIfFIFO : public CommunicationInterface
  {
  public:
    enum DRAM_REQ
    {
      DRAM_READ = 0,
      DRAM_WRITE = 1
    };
    struct DRAMReqMsg
    {
      uint64_t msgId;
      uint64_t addr;
      DRAM_REQ type;
      uint8_t data[8];
      uint16_t reqAgentId;
      uint64_t cycle;
      uint64_t dramFetchcycle;
    };

    struct DRAMRespMsg
    {
      uint64_t msgId;
      uint64_t addr;
      uint16_t wbAgentId;
      uint8_t data[8];
      uint64_t cycle;
    };

  public:
    GenericFIFO<DRAMReqMsg> m_txReqFIFO;
    GenericFIFO<DRAMRespMsg> m_rxRespFIFO;

    DRAMIfFIFO(int id, int FIFOs_depth) : CommunicationInterface(id)
    {
      m_txReqFIFO.SetFifoDepth(FIFOs_depth);
      m_rxRespFIFO.SetFifoDepth(FIFOs_depth);
    }

    virtual bool peekMessage(Message *out_msg)
    {
      if (!m_rxRespFIFO.IsEmpty())
      {
        DRAMRespMsg rx_resp = m_rxRespFIFO.GetFrontElement();
        *out_msg = {.msg_id = rx_resp.msgId,
                    .addr = rx_resp.addr,
                    .requestor_id = rx_resp.wbAgentId,
                    .cycle = rx_resp.cycle};

        out_msg->data = new uint8_t[sizeof(rx_resp.data)];
        memcpy(out_msg->data, rx_resp.data, sizeof(rx_resp.data));
        return true;
      }
      else
        return false;
    }

    virtual void popFrontMessage()
    {
      m_rxRespFIFO.PopElement();
    }

    virtual bool pushMessage(Message &msg, uint64_t cycle, double cycle_width)
    {
      if (m_txReqFIFO.IsFull())
        return false;
      DRAMReqMsg dram_msg = {.msgId = msg.msg_id,
                             .addr = msg.addr,
                             .type = (msg.complementary_value == 1) ? DRAM_REQ::DRAM_READ : DRAM_REQ::DRAM_WRITE, //ToDo: condition should be removed in the future. It is here only to convert LLCMSIProtocol::ActionId to DRAM_REQ
                             .reqAgentId = msg.requestor_id,
                             .cycle = cycle};

      m_txReqFIFO.InsertElement(dram_msg);
      return true;
    }
  };

  // Bus interface FIFO
  class InterConnectFIFO : public ns3::Object
  {
  public:
    GenericFIFO<BusIfFIFO::BusReqMsg> m_ReqMsgFIFO;
    GenericFIFO<BusIfFIFO::BusRespMsg> m_RespMsgFIFO;
  };

}

#endif /* _MemTemplate */
