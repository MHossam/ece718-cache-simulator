/*
 * File  :      CommunicationInterface.h
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On July 16, 2021
 */

#ifndef _COMMUNICATIONINTERFACE_H
#define _COMMUNICATIONINTERFACE_H

#include <stdint.h>
#include <string.h>

struct Message
{
    uint64_t msg_id; //Check if this can be removed and replaced by the addr
    uint64_t addr;
    uint64_t cycle;
    uint64_t complementary_value;
    uint16_t requestor_id;
    uint16_t responder_id;

    enum Source
    {
        LOWER_INTERCONNECT = 0,
        UPPER_INTERCONNECT
    } source;

    uint8_t *data;

    void copy(Message &M2)
    {
        memcpy(this, &M2, sizeof(M2));
        if (M2.data != NULL)
        {
            this->data = new uint8_t[8]; //TODO: constant 8 should be converted to cache line size
            memcpy(this->data, M2.data, 8);
        }
        else
            this->data = NULL;
    }

    void copy(uint8_t *data)
    {
        this->data = new uint8_t[8]; //TODO: constant 8 should be converted to cache line size
        memcpy(this->data, data, 8);
    }
};

class CommunicationInterface
{
public:
    const int m_interface_id;

    CommunicationInterface(int id) : m_interface_id(id) {}
    virtual ~CommunicationInterface() {}

    virtual bool peekMessage(Message *out_msg) = 0;
    virtual void popFrontMessage() = 0;
    virtual bool pushMessage(Message &msg, uint64_t cycle, double cycle_width) = 0;
};

#endif