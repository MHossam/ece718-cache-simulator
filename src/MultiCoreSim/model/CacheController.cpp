/*
 * File  :      CacheController.cpp
 * Author:      Mohammed Ismail
 * Email :      ismaim22@mcmaster.ca
 *
 * Created On June 23, 2021
 */

#include "CacheController.h"

namespace ns3
{
    // override ns3 type
    TypeId CacheController::GetTypeId(void)
    {
        static TypeId tid = TypeId("ns3::CacheController").SetParent<Object>();
        return tid;
    }

    // private controller constructor
    CacheController::CacheController(CacheXml &cacheXml, string &fsm_path, CommunicationInterface *upper_interface,
                                     CommunicationInterface *lower_interface, bool cach2Cache,
                                     int sharedMemId, CohProtType pType)
    {
        m_cache_cycle = 1;

        m_core_id = cacheXml.GetCacheId();
        m_shared_memory_id = sharedMemId;

        m_dt = cacheXml.GetCtrlClkNanoSec();
        m_clk_skew = m_dt * cacheXml.GetCtrlClkSkew() / 100.00;

        m_upper_interface = upper_interface;
        m_lower_interface = lower_interface;

        uint32_t cacheLines = cacheXml.GetCacheSize() / cacheXml.GetBlockSize();
        m_cache = new GenericCache(cacheLines);
        m_cache->SetCacheType(cacheXml.GetMappingType());
        m_cache->SetCacheSize(cacheXml.GetCacheSize());
        m_cache->SetCacheBlkSize(cacheXml.GetBlockSize());
        m_cache->SetCacheNways(cacheXml.GetNWays());
        m_cache->SetCacheNsets(cacheLines / cacheXml.GetNWays());

        m_cache_line_size = cacheXml.GetBlockSize();

        switch (pType)
        {
        case CohProtType::SNOOP_MSI:
            m_protocol = new MSIProtocol(m_cache, fsm_path,
                                         m_core_id, m_shared_memory_id, cacheXml.GetReqWbRatio(), cach2Cache);
            break;
        case CohProtType::SNOOP_LLC_MSI:
            m_protocol = new LLCMSIProtocol(m_cache, fsm_path, m_core_id);
            break;
        case CohProtType::SNOOP_MESI:
            m_protocol = new MESIProtocol(m_cache, fsm_path,
                                         m_core_id, m_shared_memory_id, cacheXml.GetReqWbRatio(), cach2Cache);
            break;
        case CohProtType::SNOOP_MOESI:
            m_protocol = new MOESIProtocol(m_cache, fsm_path,
                                         m_core_id, m_shared_memory_id, cacheXml.GetReqWbRatio(), cach2Cache);
            break;
        case CohProtType::SNOOP_LLC_MESI:
        case CohProtType::SNOOP_LLC_MOESI: //MOESI and MESI use the same protocol handler
            m_protocol = new LLCMESIProtocol(m_cache, fsm_path, m_core_id);
            break;
        case CohProtType::SNOOP_PMSI:
            break;
        }

        m_processing_queue =
            new FRFCFS_Buffer<Message, CoherenceProtocolHandler>(&CoherenceProtocolHandler::getRequestState,
                                                                 m_protocol,
                                                                 cacheXml.GetNPendReq());

        //initialize the actions array and the functions should be in the same order as in the enum
        m_actions_array = new ActionFunction[]{&CacheController::removePendingAndRespond,
                                               &CacheController::addtoPendingRequests,
                                               &CacheController::sendBusRequest,
                                               &CacheController::performWriteBack,
                                               &CacheController::updateCacheLine,
                                               &CacheController::saveReqForWriteBack,
                                               &CacheController::noAction};
    }

    CacheController::~CacheController()
    {
        delete m_protocol;
        delete m_cache;

        delete[] m_actions_array;
    }

    void CacheController::cycleProcess()
    {
        this->processLogic(); // Call cache controller

        Simulator::Schedule(NanoSeconds(m_dt), &CacheController::step, Ptr<CacheController>(this)); // Schedule the next run
        m_cache_cycle++;
    }

    void CacheController::init()
    {
        m_protocol->initializeCacheStates(); // Initialized Cache Coherence Protocol
        Simulator::Schedule(NanoSeconds(m_clk_skew), &CacheController::step, Ptr<CacheController>(this));
    }

    void CacheController::step(Ptr<CacheController> cache_controller)
    {
        cache_controller->cycleProcess();
    }

    void CacheController::processLogic()
    {
        this->addRequests2ProcessingQueue(*m_processing_queue);

        Message ready_msg;
        if (m_processing_queue->getFirstReady(&ready_msg) == false)
            return;

        Logger::getLogger()->updateRequest(ready_msg.msg_id, Logger::EntryId::CACHE_CHECKPOINT);

        vector<ControllerAction> actions = m_protocol->processRequest(ready_msg);

        for (ControllerAction action : actions)
            (this->*m_actions_array[action.type])(action.data);
    }

    void CacheController::addRequests2ProcessingQueue(FRFCFS_Buffer<Message, CoherenceProtocolHandler> &buf)
    {
        Message msg;

        if (m_upper_interface->peekMessage(&msg))
        {
            msg.source = Message::Source::UPPER_INTERCONNECT;
            if (buf.pushFront(msg))
                m_upper_interface->popFrontMessage();
        }

        if (m_lower_interface->peekMessage(&msg))
        {
            msg.source = Message::Source::LOWER_INTERCONNECT;
            if (buf.pushBack(msg, FRFCFS_State::NonReady))
                m_lower_interface->popFrontMessage();
        }
    }

    uint64_t CacheController::getAddressKey(uint64_t addr)
    {
        return (addr >> int(log2(this->m_cache_line_size)));
    }

    void CacheController::addtoPendingRequests(void *data_ptr)
    {
        Message *msg = (Message *)data_ptr;
        this->m_pending_cpu_requests[this->getAddressKey(msg->addr)].push(*msg);

        delete[] msg->data;
        delete msg;
    }

    void CacheController::removePendingAndRespond(void *data_ptr)
    {
        Message *msg = (Message *)data_ptr;

        if (m_pending_cpu_requests.find(getAddressKey(msg->addr)) != m_pending_cpu_requests.end() &&
            msg->msg_id == m_pending_cpu_requests[getAddressKey(msg->addr)].front().msg_id)
        {
            queue<Message> pending_messages = this->m_pending_cpu_requests[this->getAddressKey(msg->addr)];
            if (pending_messages.size() > 1)
                cout << "How !!!!!1" << endl;
            while (!pending_messages.empty())
            {
                if (msg->data != NULL)
                {
                    pending_messages.front().complementary_value = msg->complementary_value;
                    pending_messages.front().data = new uint8_t[8];
                    memcpy(pending_messages.front().data, msg->data, 8);
                }
                else //This can happen while moving from O to M
                    cout << "CacheController: Remove from pending without data" << endl;

                if (!m_lower_interface->pushMessage(pending_messages.front(), this->m_cache_cycle, this->m_dt))
                {
                    cout << "CacheController: Cannot insert the Msg into lower interface." << endl;
                    exit(0);
                }
                pending_messages.pop();
            }
            this->m_pending_cpu_requests.erase(this->getAddressKey(msg->addr));
        }
        else
        {
            if (msg->data == NULL)
            {
                GenericCache::CacheLineInfo cache_line_info = m_cache->GetCacheLineInfo(msg->addr);
                GenericCacheFrmt cache_line =
                    this->m_cache->ReadCacheLine(cache_line_info.set_idx, cache_line_info.way_idx);
                msg->copy(cache_line.data);
            }

            if (!m_lower_interface->pushMessage(*msg, this->m_cache_cycle, this->m_dt))
            {
                cout << "CacheController: Cannot insert the Msg into lower interface." << endl;
                exit(0);
            }
        }

        delete[] msg->data;
        delete msg;
    }

    void CacheController::sendBusRequest(void *data_ptr)
    {
        Message *msg = (Message *)data_ptr;

        if (!m_upper_interface->pushMessage(*msg, this->m_cache_cycle, this->m_dt))
        {
            cout << "CacheController(id = " << this->m_core_id << "): Cannot insert the Msg into the upper interface FIFO, FIFO is Full" << endl;
            exit(0);
        }

        delete[] msg->data;
        delete msg;
    }

    void CacheController::performWriteBack(void *data_ptr)
    {
        Message *msg = (Message *)data_ptr;
        if (this->m_saved_requests_for_wb.find(this->getAddressKey(msg->addr)) !=
            this->m_saved_requests_for_wb.end())
        {
            msg->requestor_id = this->m_saved_requests_for_wb[this->getAddressKey(msg->addr)].requestor_id;
            msg->msg_id = this->m_saved_requests_for_wb[this->getAddressKey(msg->addr)].msg_id;
            this->m_saved_requests_for_wb.erase(this->getAddressKey(msg->addr));
        }

        GenericCacheMapFrmt cache_line_addr = this->m_cache->CpuAddrMap(msg->addr);
        GenericCacheFrmt cache_line = this->m_cache->ReadCacheLine(cache_line_addr.idx_set);

        msg->copy(cache_line.data);

        if (!m_upper_interface->pushMessage(*msg, this->m_cache_cycle, this->m_dt))
        {
            cout << "CacheController: Cannot insert the Msg into BusTxResp FIFO, FIFO is Full" << endl;
            exit(0);
        }
        else
            cout << "DoWriteBack:: coreId = " << this->m_core_id
                 << " requested Core = " << msg->requestor_id << endl;

        delete[] msg->data;
        delete msg;
    }

    void CacheController::updateCacheLine(void *data_ptr)
    {
        GenericCacheFrmt *cache_line = (GenericCacheFrmt *)data_ptr;
        uint32_t *set_idx = (uint32_t *)((uint8_t *)data_ptr + sizeof(GenericCacheFrmt));
        uint32_t *way_idx = (uint32_t *)((uint8_t *)data_ptr + sizeof(GenericCacheFrmt) + sizeof(uint32_t));

        // if (cache_line->data != NULL) //need to be modified to check the data
        this->m_cache->WriteCacheLine(*set_idx, *way_idx, *cache_line);
        // else
        //     this->m_cache->SetCacheLineState(set_idx, way_idx, cache_line);

        delete[]((uint8_t *)data_ptr);
    }

    void CacheController::saveReqForWriteBack(void *data_ptr)
    {
        Message *msg = (Message *)data_ptr;
        this->m_saved_requests_for_wb[this->getAddressKey(msg->addr)] = *msg;

        delete[] msg->data;
        delete msg;
    }

    void CacheController::initializeCacheData(std::vector<std::string> &tracePaths)
    {
        for (string path : tracePaths)
        {
            ifstream file(path);
            string line;

            if (!file.is_open())
            {
                cout << "ERROR: Can't open trace file" << endl;
                exit(0);
            }

            while (getline(file, line))
            {
                uint64_t address;

                sscanf(line.c_str(), "%llx", &address);

                if (!this->m_cache->GetCacheLineInfo(address).IsExist)
                {
                    GenericCacheFrmt cache_line;
                    this->m_protocol->createDefaultCacheLine(address, &cache_line);
                    int set = m_cache->GetCacheLineInfo(address).set_idx;
                    int way = m_cache->GetEmptyCacheLine(set);
                    if (way == -1)
                    {
                        cout << "ERROR: Cache is not prefect ... increase the cache size" << endl;
                        exit(0);
                    }
                    this->m_cache->WriteCacheLine(set, way, cache_line);
                }
            }
            file.close();
        }
    }
}