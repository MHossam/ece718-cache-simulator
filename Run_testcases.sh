#!/bin/bash

./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/a2time01-trace" >> test1 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/aifirf01-trace" >> test2 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/basefp01-trace" >> test3 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/cacheb01-trace" >> test4 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/empty-trace" >> test5 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/iirflt01-trace" >> test6 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/pntrch01-trace" >> test7 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/rspeed01-trace" >> test8 &
./waf --run "scratch/MultiCoreSimulator --CfgFile=src/MultiCoreSim/test/myTests/tc1.xml --BMsPath=BMs/eembc-traces/ttsprk01-trace" >> test9 &